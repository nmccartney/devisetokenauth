// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require angular
//= require angular-cookie
//= require ui-router
//= require angular-bootstrap/src/accordion/accordion
//= require angular-bootstrap/src/alert/alert
//= require angular-bootstrap/src/bindHtml/bindHtml
//= require angular-bootstrap/src/buttons/buttons
//= require angular-bootstrap/src/carousel/carousel
//= require angular-bootstrap/src/collapse/collapse
//= require angular-bootstrap/src/dateparser/dateparser
//= require angular-bootstrap/src/datepicker/datepicker
//= require angular-bootstrap/src/dropdown/dropdown
//= require angular-bootstrap/src/modal/modal
//= require angular-bootstrap/src/pagination/pagination
//= require angular-bootstrap/src/popover/popover
//= require angular-bootstrap/src/position/position
//= require angular-bootstrap/src/progressbar/progressbar
//= require angular-bootstrap/src/rating/rating
//= require angular-bootstrap/src/tabs/tabs
//= require angular-bootstrap/src/timepicker/timepicker
//= require angular-bootstrap/src/tooltip/tooltip
//= require angular-bootstrap/src/transition/transition
//= require angular-bootstrap/src/typeahead/typeahead
//= require ng-token-auth
//= require_tree .



var app = angular.module('myApp', ['ui.router','ng-token-auth']);

    app.config(function($authProvider) {

        console.log('configing')

        // the following shows the default values. values passed to this method
        // will extend the defaults using angular.extend

        $authProvider.configure({
            apiUrl:                  '',
            tokenValidationPath:     '/auth/validate_token',
            signOutUrl:              '/auth/sign_out',
            emailRegistrationPath:   '/auth',
            accountUpdatePath:       '/auth',
            accountDeletePath:       '/auth',
            confirmationSuccessUrl:  window.location.href,
            passwordResetPath:       '/auth/password',
            passwordUpdatePath:      '/auth/password',
            passwordResetSuccessUrl: window.location.href,
            emailSignInPath:         '/auth/sign_in',
            storage:                 'cookies',
            proxyIf:                 function() { return false; },
            proxyUrl:                '/proxy',
            authProviderPaths: {
                //github:   '/auth/github',
                //facebook: '/auth/facebook',
                //google:   '/auth/google'
            },
            tokenFormat: {
                "access-token": "{{ token }}",
                "token-type":   "Bearer",
                "client":       "{{ clientId }}",
                "expiry":       "{{ expiry }}",
                "uid":          "{{ uid }}"
            },
            parseExpiry: function(headers) {
                // convert from UTC ruby (seconds) to UTC js (milliseconds)
                return (parseInt(headers['expiry']) * 1000) || null;
            },
            handleLoginResponse: function(response) {
                return response.data;
            },
            handleAccountResponse: function(response) {
                return response.data;
            },
            handleTokenValidationResponse: function(response) {
                return response.data;
            }
        });
    });


    //routes
    app.config(function($stateProvider,$urlRouterProvider) {

        $urlRouterProvider.otherwise("/");

        $stateProvider
            // this state will be visible to everyone
            .state('index', {
                url: '/',
                templateUrl: 'login.html',
                controller: 'IndexCtrl'
            })

            // only authenticated users will be able to see routes that are
            // children of this state
            .state('admin', {
                url: '/admin',
                abstract: true,
                template: '<ui-view/>',
                resolve: {
                    auth: function($auth) {
                        console.log('authentication');
                        return $auth.validateUser();
                    }
                }
            })

            // this route will only be available to authenticated users
            .state('admin.dashboard', {
                url: '/dash',
                templateUrl: 'dash.html',
                controller: 'AdminDashCtrl'
            });
    });


    app.controller('IndexCtrl', function($scope, $auth,$state) {
        $scope.handleRegBtnClick = function() {
            $auth.submitRegistration($scope.registrationForm)
                .then(function(resp) {
                    // handle success response
                    console.log('handle success response');
                })
                .catch(function(resp) {
                    // handle error response
                    console.log('handle error response');
                });
        };

        $auth.validateUser().then(function(resp) {
            // handle success
            $state.go('admin.dashboard')
        })

        $scope.$on('auth:login-success', function(ev, reason) {
            $state.go('admin.dashboard')
        });

        $scope.$on('auth:login-error', function(ev, reason) {
            console.log('auth:login-error', reason);
        });

    });

    app.controller('AdminDashCtrl', function($scope, $auth,$state) {
        console.log('dash')

        $scope.user = {}

        $auth.validateUser()
            .then(function(resp) {
                // handle success response
                console.log(resp)
                $scope.user = resp;
            })
            .catch(function(resp) {
                // handle error response
            });

        $scope.signout = function(){
            $auth.signOut()
                .then(function(resp) {
                    // handle success response
                    $state.go('index')
                })
                .catch(function(resp) {
                    // handle error response
                });
        }

    });
